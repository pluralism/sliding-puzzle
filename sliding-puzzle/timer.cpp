#include "timer.h"


/**
 * Default constructor of the class
 * It takes one parameter, although it's optional
 * It's default value is false as we can notice iin the header file
 * If it is set to true the stopwatch will start here
 *
 * The function also initializes running as false and total_seconds as 0
*/
Timer::Timer(bool start_now):
	running(false),
	total_seconds(0)
{
	if(start_now)
	{
		running = true;
		start_timer();
	}
}


/**
 * Function that starts the stopwatch
 * It sets running to true and it record the current time of the system clock
*/
void Timer::start_timer()
{
	running = true;
	start_time = system_clock::now();
}


/**
 * Function that stops the stopwatch and it shows an optional message to the user
 * The message is empty by default as it can be noticed in the header file
 *
 * It sets running to false and it also record the current time of the system clock
*/
void Timer::stop_timer(string optinal_message)
{
	if(!optinal_message.empty())
	{
		cout << "[*] Mensagem do cronometro: " << optinal_message << endl;
	}

	running = false;
	end_time = system_clock::now();
}


/**
 * Function that returns the number of seconds that it took to solve the puzzle
 * It only returns the real time if the stopwatch is not running!
 *
 * If it's running it returns 0(error)
*/
int Timer::elapsed_time()
{
	if(!running)
	{
		total_seconds = (int)duration_cast<std::chrono::seconds>(end_time - start_time).count();

		return total_seconds;
	}

	return 0;
}