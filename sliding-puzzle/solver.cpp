#include "solver.h"
#include "node.h"



/**
 * Default constructor
 *
 * It initializes a board(vector of vectors of Piece) in the initialState and 
 * an initial solution(usually represented as an empty string)
*/
Solver::Solver(vector<vector<Piece> > &initialState, string solution)
{
  this -> initialState = initialState;
  this -> solution = solution;
}


/**
 * This function returns a vector of int containing ALL the elements
 * of a given board(vector of vectors of Piece)
 *
 * state - board to be analyzed
*/
vector<int> Solver::extractPuzzle(vector< vector< Piece > > &state)
{
	vector<int> extracted_puzzle;

	for(size_t i = 0; i < state.size(); i++)
	{
		for(size_t j = 0; j < state[i].size(); j++)
		{
			//Extract the Piece number from the "state" and add it to the extracted_puzzle vector
			extracted_puzzle.push_back(state[i][j].getNumber());
		}
	}

	//Return the vector of int
	return extracted_puzzle;
}


/**
 * This function returns the current position of the 0
 * in the board
 *
 * board - vector of int to be analyzed
 *
 * Note that this board is different from the one where the player plays
 * This just contains the number of the pieces while the one 
 * where the player is playing contains a Piece object in each position
*/
int Solver::get_pos_zero(vector<int> &board)
{
	for(size_t i = 0; i < board.size(); i++)
		if(board[i] == 0) //Found 0
			return i;

	//Not found, return 0(error)
	return 0;
}


/**
 * Main function of this class
 *
 * It solves the board that the player is also currently playing
 * Returns the solution as a string
 *
 * Directions:
 * U - up
 * D - down
 * L - left
 * R - right
*/
string Solver::solveBoard()
{
  //Set that contains the visited_states. Sets are ideal because they do not contain duplicated elements
  set<vector<int> > visited_states;

  //Queue used to store intermediate results till we find a solution
  queue<Node> queue_states;

  //This vector contains the final solution of the puzzle
  //1 2 3 
  //4 5 6
  //7 8 0
  vector<int> final_state;
  for(size_t i = 1; i <= 8; i++)
	  final_state.push_back(i);

  final_state.push_back(0);
  
  //Add the initial state to the queue with the solution set to empty
  Node start_state = Node(extractPuzzle(initialState), "");
  queue_states.push(start_state);
  
  while(!queue_states.empty())
  {
    Node top_node = queue_states.front();
    queue_states.pop();
    
	//Analyze the top node
    if(visited_states.find(top_node.getState()) == visited_states.end())
    {
      visited_states.insert(top_node.getState());
      
      Node temporary_node = top_node;
      int zero_pos = get_pos_zero(temporary_node.getState());
      
	  /**
	  * Final puzzle: 
	  * 1 2 3 
	  * 4 5 6
      * 7 8 0
	  */

	  //If the pos is > 2 then we can swap the current piece with the one in the previous line(UP)
      if(zero_pos > 2)
      {
		//Swap the positions
		temporary_node.swapLetters(zero_pos, zero_pos - 3);

		//Add U to the solution after the swap
		temporary_node.setSolution(temporary_node.getSolution() + "U");

		//Solution found? If yes return the solution
		if(temporary_node.getState() == final_state)
			return temporary_node.getSolution();
	
		//Else add the node to the intermediate results
		queue_states.push(temporary_node);
      }
      temporary_node = top_node;
      
	  //If the pos is > 2 then we can swap the current piece with the one in the next line(DOWN)
      if(zero_pos < 6)
      {
		//Swap the positions
		temporary_node.swapLetters(zero_pos, zero_pos + 3);

		//Add D to the solution after the swap
		temporary_node.setSolution(temporary_node.getSolution() + "D");
	
		//Solution found? If yes return the solution
		if(temporary_node.getState() == final_state)
		  return temporary_node.getSolution();
	
		//Else add the node to the intermediate results
		queue_states.push(temporary_node);
      }
      temporary_node = top_node;
      
	  //If this condition is true it means that the zero is not in the left side, so we can swap 
	  //it with the number in the previous column(LEFT)
      if(zero_pos != 0 && zero_pos != 3 && zero_pos != 6)
      {
		//Swap the positions
		temporary_node.swapLetters(zero_pos, zero_pos - 1);

		//Add L to the solution after the swap
		temporary_node.setSolution(temporary_node.getSolution() + "L");
	
		//Solution found? If yes return the solution
		if(temporary_node.getState() == final_state)
			return temporary_node.getSolution();
	
		//Else add the node to the intermediate results
		queue_states.push(temporary_node);
      }
      temporary_node = top_node;
      

	  //If this condition is true it means that the zero is not in the right side, so we can swap 
	  //it with the number in the next column(RIGHT)
      if(zero_pos != 2 && zero_pos != 5 && zero_pos != 8)
      {
		//Swap the positions
		temporary_node.swapLetters(zero_pos, zero_pos + 1);

		//Add R to the solution after the swap
		temporary_node.setSolution(temporary_node.getSolution() + "R");
	
		//Solution found? If yes return the solution
		if(temporary_node.getState() == final_state)
			return temporary_node.getSolution();
	
		//Else add the node to the intermediate results
		queue_states.push(temporary_node);
      }
    }
  }

  //This only happens when we have an unsolvable puzzle
  return "Solucao nao encontrada!";
}


//Destructor...
Solver::~Solver()
{

}