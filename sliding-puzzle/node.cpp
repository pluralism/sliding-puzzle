#include "node.h"


/**
 * Default constructor of the class
 * It initializes the Node with a given state and a given solution
*/
Node::Node(vector<int> state, string solution)
{
  this -> state = state;
  this -> solution = solution;
}


//Returns the solution
string Node::getSolution()
{
  return solution;
}


//Returns the current state being examinded
vector<int> Node::getState()
{
  return state;
}


//It sets class member "solution" to another solution(the one declared in the function parameter)
void Node::setSolution(string solution)
{
  this -> solution = solution;
}


//Function that swaps two letters in the current state
void Node::swapLetters(const int l1, const int l2)
{
  char l = state[l1];
  state[l1] = state[l2];
  state[l2] = l;
}


//Destructor...
Node::~Node()
{

}