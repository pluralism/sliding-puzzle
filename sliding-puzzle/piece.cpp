#include "piece.h"



/**
 * Default constructor
 * 
 * It initializes a Piece object with a given line, column and number
 *
 * l - line of the Piece
 * c - column of the Piece
 * n - number of the Piece
*/
Piece::Piece(int l, int c, int n):
  line(l), 
  column(c), 
  number(n)
{
  
}


/**
 * Returns the line of a Piece
 * Declared as const to prevent modifications
*/
int Piece::getLine() const
{
  return line;
}


/**
 * Returns the column of a Piece
 * Declared as const to prevent modifications
*/
int Piece::getColumn() const
{
  return column;
}


/**
 * Returns the number of a piece
 * Declared as const to prevent modifications
*/
int Piece::getNumber() const
{
  return number;
}


/**
 * Function that changes the content of the class member line
 *
 * line - the new line of a Piece
 *
 * This function is useful when we're swapping two pieces
*/
void Piece::setLine(const int line)
{
  this -> line = line;
}


/**
 * Function that changes the content of the class member column
 *
 * column - the new column of a Piece
 *
 * This function is useful when we're swapping two pieces
 */
void Piece::setColumn(const int column)
{
  this -> column = column;
}


/**
 * Function that changes the content of the class member number
 *
 * number - the new number of a Piece
 *
 * This function is useful when we're swapping two pieces
 */
void Piece::setNumber(const int number)
{
  this -> number = number;
}


/**
 * Returns an ostream object and it prints the Piece data
 * It's also used when we want to print the content of a boards
*/
ostream& operator<< (ostream& o, const Piece& piece)
{
  o << piece.getNumber();
  return o;
}


//Destructor...
Piece::~Piece()
{

}