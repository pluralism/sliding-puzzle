#ifndef BOARD_H
#define BOARD_H
#include "piece.h"
#include "string_utils.h"
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::vector;
using std::time;
using std::swap;
using std::setw;
using std::setfill;



/**
 * Class Board
 *
 * This class represents a Board with a given width and height.
 * It includes several functions to deal with some aspects of the board:
 * - Move a single piece in a direction
 * - Check if the move is valid
 * - Check if the board is solved
 * - Swap pieces in a board
 * - And much more...
 * 
 * It also provides quick access to Pieces, returning a Piece object with line i, 
 * col j in a given Board board.
 *
 *
 *
 * NOTE: All the public functions are explained in board.cpp
*/


class Board
{
private:

  //width represents the width of the board(declared as unsigned because it can't be negative)
  unsigned int width;

  //height represents the height of the board(declared as unsigned because it can't be negative)
  unsigned int height;

  //DIRECTION is an enumeration of the four possible directions
  //It is used to choose a direction between the 4 that are possible to swap the pieces when shuffling the board
  typedef enum { UP = 0, 
		 LEFT = 1, 
		 DOWN = 2, 
		 RIGHT = 3 } DIRECTION;


  /**
   * string_utils object to deal with string operations:
   * - Conversion between types
   * - Digit extraction from user input
   * - Extraction of the size of a board
   * - And some helper functions
  */
  string_utils str_utils;
  
  
public:
  vector<vector<Piece> > board;

  Board();
  Board(const unsigned int w, const unsigned int h);
  Board(vector<vector<Piece> > &board);
  int getBoardDimension() const;
  vector<vector<Piece> > buildBoard();
  bool isValidMove(unsigned int i, unsigned int j) const;
  vector<Piece> getValidMoves(vector<vector<Piece> > &board);
  Piece& getPiece(int number, vector<vector<Piece> > &board);
  Piece& getPiece(const int i, const int j, vector<vector<Piece> > &board);
  void swapPieces(Piece &p1, Piece &p2);
  void moveUp();
  void moveDown();
  void moveLeft();
  void moveRight();
  const bool isBoardSolved(vector<vector<Piece> > &board);
  string solveBoard();
  bool isOnValidMoves(const int number, vector<Piece> &board);
  friend ostream& operator<< (ostream& o, const Board& board);
  virtual ~Board();
};

#endif // BOARD_H