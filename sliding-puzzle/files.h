#ifndef FILES_H
#define FILES_H
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "string_utils.h"
#include "game.h"

extern "C" {
	#include <io.h>
	#include <sys/stat.h>
}

#define FILE_BASE "puzzle_"

using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::string;
using std::sort;
using std::vector;
using std::ofstream;
using std::ifstream;
using std::ostringstream;
using std::istringstream;
using std::to_string;
using std::ios;



/**
 * Class Files
 *
 *
 * This class is used to manipulate files
 * It has implemented a lot of funtions
 * - checks if a file exists
 * - create the formated stats to a puzzle
 * - write player objects to a file
 * - creates new files with a puzzle and a vector of players
 * - it is able to create a player ranking
 * - shows all the content of a file
 * - return the number of puzzles of a given size in a puzzle
 * - update the number of puzzles in a file
 * - build a vector with a puzzle that is a file
 * - much more...
 *
 *
 *
 * NOTE: All the public functions are commented in the file files.cpp
 * [IMPORTANT] NOTE2: Sometimes we had problems while compiling the project:
 * - when the program claims about a missing ";" just comment line 66 of game.h, compile, uncomment and compile again
*/


class Files {

	//String_utils object used to deal with string
	string_utils str_utils;

	//Each puzzle in PUZZLE_NUMBERS has a type and a number of puzzle(total_played)
	struct Puzzle {
		string type;
		string total_played;
	};

public:
	Files();
	bool file_exists(const char *filename);
	bool create_file(const char *filename);
	void create_zero_stats(const char *filename);
	void show_file_error(const char *filename);
	vector<struct Puzzle> get_all_puzzles(const char *filename);
	void update_puzzle(const int size, const char *filename);
	void create_new_puzzle_file(vector<vector<Piece> > &board, int size, Player &player);
	vector<Player> get_all_players_file(const char *filename);
	int get_puzzle_file_size(const char *filename);
	void write_puzzle_data(const char *filename, vector<vector<Piece> > &board);
	void write_player_data(const char *filename, Player &player);
	void add_player_to_file(const char *filename, Player &player);
	int get_number_of_puzzles(const int size, const char *filename);
	static bool sort_file_numbers(const Puzzle& first, const Puzzle& second);
	static bool sort_player_rank(const Player& first, const Player& second);
	vector<vector<Piece> > extract_puzzle_file(const char *filename);
	int get_zeros_pos(const char *filename, const int puzzle_size);
	vector<int> get_number_line(const char *filename, const int puzzle_size, const int max_diff, const int pos);
	void show_file(const char *filename);
	bool file_contains_standard_sizes(const char *filename);
};

#endif //FILES_H