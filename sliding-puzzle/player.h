#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <cstdlib>
using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::setw;
using std::ostream;
using std::remove;



/**
 * Class Player
 *
 *
 * This class is responsible to manage Player objects
 * Each Player has the following data:
 * 
 * - name
 * - age
 * - sex
 * - time
 *
 * Each time a Player solves a puzzle in competition mode it's asked the player data and this data
 * is recorded into a new/existing file
 *
 *
 * NOTE: All the public functions are explained in player.cpp
*/


class Player
{
private:
  string name;
  unsigned int age;
  char sex;
  unsigned int time;
  
public:
  Player(string name, unsigned int age, char sex, unsigned int time);
  string getName() const;
  unsigned int getAge();
  char getSex() const;
  const unsigned int getTime() const;
  bool validAge();
  void format_name();
  friend ostream& operator<< (ostream& o, Player& player);
  virtual ~Player();
};

#endif // PLAYER_H
