#include "board.h"
#include "solver.h"



/**
 * This is the default class constructor
 * It initializes a default board object width both
 * width and height set to 3
*/
Board::Board():
  width(3),
  height(3)
{

}


/**
 * This is an alternative constructor that takes two parameters:
 * w - width of the board
 * h - height of the board
 *
 * When the user chooses for example "3x4" it prints an error message and exits
 * This constructor can be used in future version if we want to have rectangular puzzles(by removing the if condition)
*/
Board::Board(const unsigned int w, const unsigned int h):
  width(w),
  height(h)
{
  if(w != h)
  {
    cerr << "[!] Erro: O tabuleiro tem que estar no formato NxN!" << endl;
    exit(EXIT_FAILURE);
  }
}



/**
 * A thirt constructor that takes a vector of vectors of Piece as a parameter:
 * board - A vector of vectors of Piece 
 * It initializes the Board from the Board class with the board from the parameter
 * It is useful for example when we want to load a puzzle from a file.
*/
Board::Board(vector<vector<Piece> > &board):
	width(board.size()),
	height(board.size())
{
	for(size_t i = 0; i < board.size(); i++)
	{
		vector<Piece> row;

		for(size_t j = 0; j < board[i].size(); j++)
		{
			row.push_back(board[i][j]);
		}
		this -> board.push_back(row);
	}
}


/**
 * This functions takes no parameters and returns a string with the solution for 
 * the current board.
 * It should be noted that it only solves 3x3 puzzles!
 * Bigger puzzles would require another algorithm(like IDA*)
*/
string Board::solveBoard()
{
  string solution;

  if(width == 3)
  {
    Solver solver = Solver(board, "");
	cout << "[*] A resolver o puzzle... Pode demorar ate 45s!" << endl;
    solution = solver.solveBoard();
  
  } else {
    cerr  << "[!] Only 3x3 puzzles can be solved by the computer!"  << endl;
  }

  return solution;
}



/**
 * Function to check if the given board in the parameter is solved
 * board - board to be checked
 * 
 * Returns true if the board is solved, else if returns false
 * With a board in the parameter we can check ANY puzzle, it just needs to be a vector of
 * vectors of Piece
 * Declared as const to prevent acidental modifications
*/
const bool Board::isBoardSolved(vector<vector<Piece> > &board)
{
  //max is the piece in the first position
  int max = getPiece(0, 0, board).getNumber();
  
  //if the first position is 0 then it immediately return false
  if(max == 0)
    return false;
  
  for(size_t i = 0; i < board.size(); i++)
  {
    vector<Piece> row;
    
    for(size_t j = 0; j < board[i].size(); j++)
    {
	  //If we are in the last position then the number should be 0
	  //It doesn't need to be checked
	  //If all the others are in ascending order, then it is guarented that the puzzle is solved
      if((i == board.size() - 1 && j == board.size() - 1))
		continue;
      
	  //Not in asceding order, return false
      if(getPiece(i, j, board).getNumber() < max)
      {
		return false;
      } else max = getPiece(i, j, board).getNumber(); //Max value is the current Piece, we're going well(ascending order)...
    }
  }

  //All numbers are in ascending order, return true
  return true;
}


/**
 * This function returns a Piece object in a given board
 * number - number we want to search in the board
 * board  - board where we'll be searching for the number
*/
Piece& Board::getPiece(const int number, vector<vector<Piece> > &board)
{
  for(size_t i = 0; i < board.size(); i++)
  {
    vector<Piece> row;
    
    for(size_t j = 0; j < board[i].size(); j++)
	  //If the number of the piece [i][j] from the board is equal to the number then 
	  //we've found the right number
	  //Return the piece that is in the position [i][j]
      if(board[i][j].getNumber() == number)
		return board[i][j];
  }

  //Piece not found, this should not happen!
  return board[0][0];
}


/**
 * Check if a move is valid(it needs to be inside the limits of the board)
 * i - line
 * j - column
*/
bool Board::isValidMove(unsigned int i, unsigned int j) const
{
  //Move is valid!
  if(i >= 0 && i < width && j >= 0 && j < height)
    return true;
  
  //Should not happen, it meens the move is not valid!
  return false;
}


/**
 * Searches for a piece in line i, col j and Board board
 * It returns the Piece object in the position [i][j]
 * i - line to search
 * j - column to search
 * board - board where we'll be searching
*/
Piece& Board::getPiece(const int i, const int j, vector<vector<Piece> > &board)
{
  return board[i][j];
}


/**
 * This function is used to swap two pieces in the Board from the class
 * p1 - first piece
 * p2 - second piece
 * 
 * It starts by swapping the line and column of the pieces
 * and finally it swaps the Piece objects.
*/
void Board::swapPieces(Piece &p1, Piece &p2)
{
  Piece p = p1;
  p1.setLine(p2.getLine());
  p1.setColumn(p2.getColumn());
  p2.setLine(p.getLine());
  p2.setColumn(p.getColumn());

  //swap function from the STL library
  swap(p1, p2);
}


/**
 * Function that takes no arguments
 * It moves a piece for the line above by using the swap function defined above
*/
void Board::moveUp()
{
  swapPieces(getPiece(0, board), getPiece(getPiece(0, board).getLine() - 1, getPiece(0, board).getColumn(), board));
}


/**
 * Function that takes no arguments
 * It moves a piece for the line below by using the swap function defined above
*/
void Board::moveDown()
{
  swapPieces(getPiece(0, board), getPiece(getPiece(0, board).getLine() + 1, getPiece(0, board).getColumn(), board));
}


/**
 * Function that takes no arguments
 * It moves a piece for the next column by using the swap function defined above
*/
void Board::moveRight()
{
  swapPieces(getPiece(0, board), getPiece(getPiece(0, board).getLine(), getPiece(0, board).getColumn() + 1, board));
}


/**
 * Function that takes no arguments
 * It moves a piece for the previous column by using the swap function defined above
*/
void Board::moveLeft()
{
  swapPieces(getPiece(0, board), getPiece(getPiece(0, board).getLine(), getPiece(0, board).getColumn() - 1, board));
}


/**
 * One of the main functions from this class
 * It is responsible for building a board with an already defined
 * width and height
 * 
 * It starts with a solved puzzle and then it swap the pieces
 * x times till we get a shuffled board
*/
vector<vector<Piece> > Board::buildBoard()
{
  //pseudo-random number generator
  //the clock time from the PC is passed as the seed
  //Number is casted to a unsigned int(positive int values, including 0)
  srand(static_cast<unsigned int>(time(NULL)));
  
  for(size_t i = 0; i < width; i++)
  {
    vector<Piece> row;
    
    for(size_t j = 0; j < height; j++)
    {
	  //This is responsible for building a solved board
	  //If we are in the last position of the for loop we add a 0, 
	  //else we keep adding numbers to the vector in ascending order
      row.push_back(Piece(i, j, ((j == width - 1) && (i == height - 1) ? 0 : i * width + j + 1)));
    }  
	//Add a vector of Pieces to the board vector
    board.push_back(row);
  }
    
  
  for(int i = 0; i < 1500; i++)
  {
	//This returns a DIRECTION between 0 and 4(one of the 4 that are possible)
    DIRECTION dir = static_cast<DIRECTION>(UP + (rand() % 4));
    
	//If the direction is UP and the move is valid then it swaps the pieces
    if(dir == UP && isValidMove(getPiece(0, board).getLine() - 1, getPiece(0, board).getColumn()))
    {
      moveUp();
    } //If the direction is RIGHT and the move is valid then it swaps the pieces
	else if(dir == RIGHT && isValidMove(getPiece(0, board).getLine(), getPiece(0, board).getColumn() + 1)) {
      moveRight();
    } //If the direction is DOWN and the move is valid then it swaps the pieces 
	else if(dir == DOWN && isValidMove(getPiece(0, board).getLine() + 1, getPiece(0, board).getColumn())) {
      moveDown();
    } //If the direction is LEFT and the move is valid then it swaps the pieces 
	else if(dir == LEFT && isValidMove(getPiece(0, board).getLine(), getPiece(0, board).getColumn() - 1)) {
      moveLeft();
    }
  }
  
  //Return a newly shuffled board
  return board;
}


/**
 * This function starts by searching for the 0 in the board
 * and then it analyzes all the 4 possible directions
 * surrounding the 0 Piece
 *
 * board - board where we'll be analyzing the Pieces
 *
 * It returns a vector of Pieces containing the possible moves
*/
vector<Piece> Board::getValidMoves(vector<vector<Piece> > &board)
{
  int current_line = 0;
  int current_column = 0;
  vector<Piece> validMoves;
  
  for(size_t i = 0; i < board.size(); i++)
  {
    vector<Piece> row;
    
    for(size_t j = 0; j < board[i].size(); j++)
    {
	  //We found the 0, save the line and col in current_line and current_column
      //Break the loop, there is no need to keep searching
      if(board[i][j].getNumber() == 0)
      {
		current_line = i;
		current_column = j;
		break;
      }
    }
  }
  
  //If the user can move a piece UP add to the vector
  if(isValidMove(current_line - 1, current_column))
	  validMoves.push_back(board[current_line - 1][current_column]);
  
  //If the user can move a piece LEFT add to the vector
  if(isValidMove(current_line, current_column - 1))
	  validMoves.push_back(board[current_line][current_column - 1]);
  
  //If the user can move a piece RIGHT add to the vector
  if(isValidMove(current_line, current_column + 1))
	  validMoves.push_back(board[current_line][current_column + 1]);
  
  //If the user can move a piece DOWN add to the vector
  if(isValidMove(current_line + 1, current_column))
	  validMoves.push_back(board[current_line + 1][current_column]);
  

  //Returns the Piece vector
  return validMoves;
}


/**
 * Function to search for a number in a vector of Pieces
 * 
 * number - number to search
 * vector to search
 * 
 * It returns true if the number was found, meaning that the number
 * is in the possible moves
*/
bool Board::isOnValidMoves(const int number, vector<Piece> &board)
{
	for(size_t i = 0; i < board.size(); i++)
	{
		//Number found
		if(board[i].getNumber() == number)
			return true;
	}

	//Number was NOT found
	return false;
}



/**
 * Returns an ostream object and prints the board
 * We overloaded the operator <<, responsible for printing the board
*/
ostream& operator<< (ostream& o, const Board& board)
{

  if(board.board.size() > 0)
  {    
    for(size_t i = 0; i < board.board.size(); i++)
    {
      vector<Piece> row;
      for(size_t j = 0; j < board.board[i].size(); j++)
      {
		o << setw(board.str_utils.getNumberOfDigits((int)pow(board.width, 2) - 1)) << "|";

		//If the number of the board is 0, we print a space
		if(board.board[i][j].getNumber() == 0)
			o << " ";
		else
			o << board.board[i][j]; //Else print the current number
		

		//If the number of digits of the current number(2 for the number 16 for example)
		//is less than the maximum number of digits in that board
		//then we have to adjust the width with the setw(int n) function by adding till 
		//we get a width of the maximum number of digits
		//This assures us that the width will be the same for all numbers
		if(board.str_utils.getNumberOfDigits(board.board[i][j].getNumber()) < board.str_utils.getNumberOfDigits((int)pow(board.width, 2)))
		{
			int wid = board.str_utils.getNumberOfDigits((int)pow(board.width, 2) - 1) - board.str_utils.getNumberOfDigits(board.board[i][j].getNumber()) + 1;
			o << setw(wid);
		}
		o << "|";
      }
	  //Change line
      o << endl;
    }
  } else {
    cerr << "[!] Tabuleiro nao inicializado!" << endl;
    exit(EXIT_FAILURE);
  }
  
  return o;
}


//Returns the board dimension
//We used const because the function will not modify the value
int Board::getBoardDimension() const
{
  return width;
}


//Destructor...
Board::~Board()
{

}