#ifndef SOLVER_H
#define SOLVER_H
#include <vector>
#include <queue>
#include <string>
#include <algorithm>
#include <set>
#include <iostream>
#include "piece.h"
#include "board.h"
using namespace std;



/**
 * Class Solver
 *
 *
 * This class is the responsible for solving a board
 * It can only solves 3x3 puzzles because bigger puzzles would require different
 * algorithms
 * We have implement BFS to solve the puzzle, altough the performance could be improved
 * with the A* algorithm(using an heuristic function as a guide)
 *
 *
 *
 * NOTE: All public functions are described in the file solver.cpp
*/


class Solver
{
public:
  string solution;
  vector<vector<Piece> > initialState;
  Solver(vector<vector<Piece> > &initialState, string solution);
  vector<int> extractPuzzle(vector<vector<Piece> > &state);
  int get_pos_zero(vector<int> &board);
  bool checkPuzzle(vector<vector<Piece> > &b1, vector<vector<Piece> > &b2);
  string solveBoard();
  virtual ~Solver();
};

#endif // SOLVER_H