#include "game.h"



/**
 * Default constructor
 * When initialized it does nothing
*/
Game::Game()
{

}


/**
 * Simple function used to show the logo of the game to the player
 * It returns nothing(void)
*/
void Game::show_game_logo()
{
	cout << "....................................................................." << endl;
    cout << "...////////..//.........//../////......//..////......//...////////..." << endl;
    cout << "..//.........//.........//..//...//....//..//.//.....//..//.........." << endl;
    cout << "..//.........//.........//..//....//...//..//..//....//..//.........." << endl;
    cout << "../////////..//.........//..//.....//..//..//...//...//..//.../////.." << endl;
    cout << ".........//..//.........//..//....//...//..//....//..//..//......//.." << endl;
    cout << ".........//..//.........//..//...//....//..//.....//.//..//......//.." << endl;
    cout << "..////////.../////////..//../////......//..//......////...////////..." << endl;
    cout << "....................................................................." << endl;

    cout << "..////////...//.....//../////////../////////..//........./////////..." << endl;
    cout << "..//.....//..//.....//........//.........//...//.........//.........." << endl;
    cout << "..//.....//..//.....//.......//.........//....//.........//.........." << endl;
    cout << "..////////...//.....//......//.........//.....//.........//////......" << endl;
    cout << "..//.........//.....//.....//.........//......//.........//.........." << endl;
    cout << "..//.........//.....//....//.........//.......//.........//.........." << endl;
    cout << "..//..........///////.../////////../////////../////////../////////..." << endl;
    cout << "....................................................................." << endl;
}



/**
 * Function that check if PUZZLE_NUMBERS exists
 * If exists it will then check for the standard sizes:
 * - 3x3
 * - 4x4
 * - 5x5
 *
 * If the file doesn't exists it'll automatically create it and add the standard sizes to it
*/
void Game::load_stats()
{
	//If the file doesn'e exists
	if(!(files.file_exists(PUZZLE_NUMBERS)))
	{
		//Create it!
		files.create_file(PUZZLE_NUMBERS);
	}

	//If the file doesn't have the standard sizes
	//the program displays an error message to the user and the file is rewritten
	if(!files.file_contains_standard_sizes(PUZZLE_NUMBERS))
	{
		cout << "[*] \"puzzle_numbers.txt\" nao esta no formato correcto! A reescrever o ficheiro..." << endl << endl;
		files.create_zero_stats(PUZZLE_NUMBERS);
	}

	//Proccedd to the next step...
	askUserStart();
}


/**
 * Function that displays to the user "limit" puzzles 
 * with size "size" to the user
 *
 * If for some reason there is a problem with the files the 
 * user will be notified and asked to recreate the files
*/
void Game::show_all_puzzles(const int size, const int limit)
{
	string file_name;

	for(int i = 1; i <= limit; i++)
	{
		//Build the file name so we can search it in a directory
		//to_string(int n) is part of C++11
		file_name = FILE_BASE + to_string(size) + "x" + to_string(size) + "_" + str_utils.format_puzzle_number(i) + ".txt";

		//If the file exists then it opens the file and show it to the user
		if(files.file_exists(file_name.c_str()))
		{
			cout << "Ficheiro " << i << ": \"" << file_name << "\"" << endl;
			files.show_file(file_name.c_str());

			//This line works as a delimiter between the puzzles
			cout << endl << "------------------------------------------------" << endl << endl;
		} else {
			//Something went wrong
			//Display an error message and exit with EXIT_FAILURE code
			cerr << "[!] Erro! O ficheiro \"" << file_name << "\" nao existe!" << endl;
			cerr << "[!] Verifique os ficheiros. Tera que ter " 
				<< files.get_number_of_puzzles(size, PUZZLE_NUMBERS) << " puzzles do tamanho " << size << "." << endl;

			getchar();
			exit(EXIT_FAILURE);
		}
	}
}


/**
 * Function that asks a user to select a puzzle of size "size"
 * 
 * size  - size of the puzzle
 * limit - maximum number the player can choose
*/
string Game::select_puzzle_from_files(const int size, const int limit)
{
	string file_name;
	string choice;

	cout << "[*] Escolher puzzle(1.." << limit << ")" << endl;
	getline(cin, choice);


	//Keep asking the user till he/she chooses a valid number
	while(atoi(choice.c_str()) < 1 || atoi(choice.c_str()) > limit)
	{
		cout << "[!] Opcao invalida. Seleccionar novamente: ";
		getline(cin, choice);
	}

	//At this point the user entered a valid number
	//This means we can build the file name and return it
	file_name = FILE_BASE + to_string(size) + "x" + to_string(size) + "_" + str_utils.format_puzzle_number(atoi(choice.c_str())) + ".txt";

	//Return the file name
	return file_name;
}


/**
 * Function that checks if a string contains invalid data or not
 * s - string to be analyzed
 *
 * returns true if the string at least a single non digit char
*/
bool Game::contains_invalid_char(string &s)
{
	for(size_t i = 0; i < s.size(); i++)
	{
		//Invalid
		if(!isdigit(s[i]))
			return true;
	}

	//String is valid
	return false;
}



/**
 * Function responsible for asking the user about the puzzle size
 * and game mode
*/
void Game::askUserStart()
{
	show_game_logo();

	//If user_choice as some content delete it
	user_choice = "";

	//Prompts the user to choose a puzzle size
	cout << "[*] Seleccionar um puzzle: 3x3, 4x4 ou 5x5!" << endl;
	getline(cin, user_choice);

	//Keep asking the user till we get an valid answer
	while(user_choice.empty())
	{
		cout << "[!] Deve seleccionar um puzzle valido... Seeccionar novamente: ";
		getline(cin, user_choice);
	}

	//Just in case game_mode as some content
	game_mode = "";

	//Prompts the user to choose a puzzle size
	cout << "[*] Em que modo pretende jogar?((C)ompeticao/(T)reino/(S) para sair)" << endl;
	getline(cin, game_mode);
	str_utils.convert_upper(game_mode);


	//If the user chooses S quit game with exit code EXIT_SUCCESS
	if(game_mode == "S")
	{
		getchar();
		exit(EXIT_SUCCESS);
	}

	//Keep asking the user till we get an valid answer
	while(game_mode != "C" && game_mode != "T" && game_mode != "S")
	{
		cout << "[!] Modo invalido... Seleccionar novamente: ";
		getline(cin, game_mode);
		str_utils.convert_upper(game_mode);

		//If the user chooses S quit game with exit code EXIT_SUCCESS
		if(game_mode == "S")
		{
			getchar();
			exit(EXIT_SUCCESS);
		}	
	}


	//Go to the next step....
	prepareGame();
}


//Function used to clear the screen
void Game::clear_screen()
{
	for(int i = 0; i < 100; i++)
		cout << endl;

	//Clear the buffer at the end
	cout.clear();
}


/**
 * One of the main functions of the puzzle
 * It asks the user to choose a puzzle if the user chose competition mode previously
 * It can load a puzzle from a file or create a new puzzle in competition mode
*/
void Game::prepareGame()
{
	//Variable used to hold the type of puzzle(old/new)
	string list_puzzle;

	//Variable used to hold the name of the file to load
	string file_to_load;
	dimensions = str_utils.format_string(user_choice);

	//Variable that holds the number of puzzles of dimensions[0]
	int puzzle_number = files.get_number_of_puzzles(str_utils.convertString(dimensions[0]), PUZZLE_NUMBERS);

	//If the user choose competation mode...
	if(game_mode == "C")
	{
		//This means we have got some data
		if(puzzle_number != -1)
		{
			//Display an information message to the user
			cout << endl << "[*] De acordo com o ficheiro \"" << PUZZLE_NUMBERS << "\" existem " 
				<< puzzle_number << " puzzles com o tamanho " << dimensions[0] << "." << endl;


			cout << "[*] Carregar puzzle antigo(L) / Gerar um novo puzzle(N)" << endl;
			getline(cin, list_puzzle); //Hold the user answer(old puzzle/new puzzle)
			str_utils.convert_upper(list_puzzle); //Conver the answer to uppercase

			//If the user chose S de program should quit
			if(list_puzzle == "S")
			{
				if(b1 != NULL)
					delete b1;

				//"Reset" the game
				clear_screen();
				load_stats();
			}

			//Keep asking the user until he/she enters a valid answer
			while(list_puzzle != "L" && list_puzzle != "N")
			{
				cout << "[*] Opcao invalida! Seleccionar novamente: ";
				getline(cin, list_puzzle); //Hold the user answer
				str_utils.convert_upper(list_puzzle);


				//If the user chose S de program should quit
				if(list_puzzle == "S")
				{
					if(b1 != NULL)
					delete b1;

					//"Reset" the game
					clear_screen();
					load_stats();
				}
			}


			//If list puzzle is L it means that the user wants to load an old puzzle
			if(list_puzzle == "L")
			{
				//We have no puzzles of that size
				//Display an error message and quit
				if(puzzle_number == 0)
				{
					cerr << "[!] Nao existem puzzles disponiveis! A sair..." << endl;
					getchar();
					exit(EXIT_FAILURE);
				}

				//If the previous condition was false then we have some puzzles
				//Show all of them(size is dimensions[0])
				show_all_puzzles(str_utils.convertString(dimensions[0]), puzzle_number);


				//This prompts user to choose a file and return the file name
				file_to_load = select_puzzle_from_files(str_utils.convertString(dimensions[0]), puzzle_number);

				//Display the filename of the chose puzzle
				cout << "Escolheu o ficheiro " << file_to_load << endl;

				try{
					//Extract the puzzle from the puzzle and put it in a vector of vectors of Piece
					vector<vector<Piece> > board = files.extract_puzzle_file(file_to_load.c_str());

					if(b1 != NULL)
						b1 = NULL;

					if(b1 == NULL)
						b1 = new Board(board);
				}catch(bad_alloc &alloc) {
					//Should not happen!
					cerr << "[!] Ocorreu um problema ao alocar memoria para um novo puzzle!" << alloc.what() << endl;
				}
			} else {
				//Display an information message to the user
				cout << "[*] O programa vai gerar um puzzle automaticamente no modo competicao..." << endl << endl;

				try{
					if(b1 != NULL)
						delete b1;

					b1 = new Board(str_utils.convertString(dimensions[0]), str_utils.convertString(dimensions[1]));
					b1 -> buildBoard();
				}catch(bad_alloc &alloc) {
						//Should not happen!
						cerr << "[!] Ocorreu um problema ao alocar memoria para um novo puzzle!" << alloc.what() << endl;
				}
			}
		} else 
		{
			//Insert this file size in the PUZZLE_NUMBERS file
			files.update_puzzle(atoi(dimensions[0].c_str()), PUZZLE_NUMBERS);

			//Run this function again
			prepareGame();
		}
	} else if(game_mode == "T") //User chose training mode!
	{
		if(b1 != NULL)
			delete b1;
		
		b1 = new Board(str_utils.convertString(dimensions[0]), str_utils.convertString(dimensions[1]));
		b1 -> buildBoard();
	}

	//Save the initial state of board in original_board so we can save it later to a file
	original_board = b1 -> board;

	//Go to the next step...
	play_game(game_mode, list_puzzle, file_to_load);
}


/**
 * This function asks a user to write his/her information
 * so it can be saved to a file
 *
 * time - the time a user took to a solve the puzzle
*/
Player Game::ask_user_data(unsigned int time)
{
	string name;
	string final_name;
	char sex;
	string age;

	cout << "[*] Escreva o seu nome(primeiro/ultimo) (numeros nao serao considerados): ";
	getline(cin, name);

	//Delete invalid chars from the string
	for(size_t i = 0; i < name.size(); i++)
	{
		if(isalpha(name[i]) || isspace(name[i]))
			final_name += name[i];
	}


	while(name.empty())
	{
		cout << "[!] O nome nao pode estar vazio! Reescrever..." << endl;
		getline(cin, name);

		for(size_t i = 0; i < name.size(); i++)
		{
			if(isalpha(name[i]) || isspace(name[i]))
				final_name += name[i];
		}
	}


	cout << "[*] Escreva a sua idade: ";
	getline(cin, age);

	//Keep asking the user until he/she enters valid information
	while(contains_invalid_char(age))
	{
		//Display an error message to the user and ask him/her again
		cout << "[!!] Idade inserida no formato errado. Reescrever por favor...\n";
		getline(cin, age);
	}

	cout << "[*] Insira o seu sexo(m/f): ";
	cin >> sex;

	//Keep asking the user until he/she enters valid information
	while(sex != 'm' && sex != 'M' && sex != 'f' && sex != 'F')
	{
		//Display an error message to the user and ask him/her again
		cout << "[!!] Caracter invalido... Reescrever por favor: ";
		cin >> sex;
	}

	//Discard the characters
	cin.clear();
	cin.ignore(INT_MAX, '\n');


	//Returns a new Player object with a name, age, sex and time
	return Player(final_name, atoi(age.c_str()), toupper(sex), time);
}


/**
 * Function used to show the formatted time to the user
 * If the user took more than 1 minute to solve the puzzle
 * it formats the time in minutes:seconds
 *
 * It adds 2 seconds to the final time for each used hint
*/
void Game::show_formatted_time(Timer t, const int used_hints)
{
	//More than one minute?
	//If true format the time in minutes:seconds
	if(t.elapsed_time() > 60)
	{
		int time = t.elapsed_time();
		int extra = used_hints * 2;
		time += extra; //Add the extra time to the final time

		int minutes = time / 60;
		int seconds = time % 60;

		//Display the time a user took to solve the puzzle
		cout << "[INFO] Resolveu o puzzle em " << minutes << "m e " << seconds << "s... ";
		if(used_hints > 0)
			cout << "(" << used_hints << " pistas utilizadas)"; //Inform the user about how many he/she it used
		cout << endl << endl;

	} else {
		//If the user solved a puzzle in less than one minute is just shows the seconds
		cout << "[INFO] Resolveu o puzzle em " << t.elapsed_time() << "s..." << endl << endl;
	}
}



/**
 * Function that asks the user for a new number so the board can swap two pieces
*/
string Game::ask_user_number()
{
	string s;
	getline(cin, s);

	if(s == "pista")
		return "-1";

	//Kep asking the user until we get a valid answer
	while(contains_invalid_char(s))
	{
		cout << "[!!] Caracteres invalidos... Escolha novamente..." << endl;
		getline(cin, s);
		if(s == "pista")
			return "-1";
	}

	//Return the user valid answer
	return s;
}


/**
 * Main function of this class
 * It runs while the user is playing
 *
 * mode - game mode(competition/training)
 * type - old puzzle(load)/new puzzle
 * file_to_load - if we're playing an old puzzle it loads from this puzzle
*/
void Game::play_game(string mode, string type, string file_to_load)
{
	Timer timer;

	//This variable holds the solution if the user chose "pista"
	string solution;
	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO screen_info;
	unsigned short oldColor;
	int used_hints = 0;

	GetConsoleScreenBufferInfo(stdOut, &screen_info);
	oldColor = screen_info.wAttributes;

	//Start the stopwatch
	timer.start_timer();

	//If the user is trying to beat a time show him/her the current best time
	//And the name of the player who did it
	if(mode == "C" && type == "L")
	{
		vector<Player> players = files.get_all_players_file(file_to_load.c_str());
		if(players.size() > 0)
		{
			cout << "[*] O tempo a bater e: ";
			SetConsoleTextAttribute(stdOut, GREEN_TEXT | FOREGROUND_INTENSITY);
			cout << players[0].getTime() <<  "s" << endl;
			SetConsoleTextAttribute(stdOut, oldColor);
			cout << "[*] E pertence a: ";
			SetConsoleTextAttribute(stdOut, GREEN_TEXT | FOREGROUND_INTENSITY);
			cout << players[0].getName() << endl;
			SetConsoleTextAttribute(stdOut, oldColor);
		}
	}
	
	//Display the puzzle to the user
	cout << *b1 << endl;
	
	//While the board is not solved
	//Keep asking the user for a valid move
	while(!(b1 -> isBoardSolved(b1 -> board)))
	{
		int user_choice;

		//vector of Piece that contains the pieces the user can move at this point
		valid_moves = b1 -> getValidMoves(b1 -> board);

		cout << "[*] Lista de opcoes validas para esta jogada: " << endl;

		//Display each possible move to the user
		for(size_t i = 0; i < valid_moves.size(); i++)
		{
			SetConsoleTextAttribute(stdOut, BLUE_TEXT | FOREGROUND_INTENSITY);
			cout << valid_moves[i] << endl;
			SetConsoleTextAttribute (stdOut, oldColor);
		}

		cout << "[*] Seleccione uma das opcoes(ou \"pista\" para mostrar uma parte da solucao): " << endl;

		//Ask the user to select a move
		user_choice = atoi(ask_user_number().c_str());

		//User chose "pista"
		if(user_choice == -1)
		{
			//Solve the board and hold it in a string
			solution = b1 -> solveBoard();

			//Increment the used_hints variable
			if(b1 -> getBoardDimension() == 3)
				used_hints++;

			//Each time a user chooses "pista" the program shows a maximum of 4 steps(if available)
			if(b1 -> getBoardDimension() == 3)
			{
				cout << "Pistas: ";
				for(size_t i = 0; i < (solution.size() >= 4 ? 4 : solution.size()); i++)
				{
					SetConsoleTextAttribute(stdOut, BLUE_TEXT);
					cout << solution[i] << " ";
					SetConsoleTextAttribute(stdOut, oldColor);
				}
			}
			cout << endl;
			continue;
		}

		//Keep asking the user until he/she selects a move that is the valid moves vector
		while(!(b1 -> isOnValidMoves(user_choice, valid_moves)))
		{
			SetConsoleTextAttribute(stdOut, BLUE_TEXT);
			cout << "[!!] Opcao invalida!" << endl;
			SetConsoleTextAttribute (stdOut, oldColor);

			//Ask the user to select a move
			user_choice = atoi(ask_user_number().c_str());

			//User chose "pista"
			if(user_choice == -1)
			{
				//Solve the board and hold it in a string
				solution = b1 -> solveBoard();

				//Increment the used_hints variable
				if(b1 -> getBoardDimension() == 3)
					used_hints++;

				//Each time a user chooses "pista" the program shows a maximum of 4 steps(if available)
				if(b1 -> getBoardDimension() == 3)
				{
					cout << "Pistas: ";
					for(size_t i = 0; i < (solution.size() >= 4 ? 4 : solution.size()); i++)
					{
						SetConsoleTextAttribute(stdOut, BLUE_TEXT);
						cout << solution[i] << " ";
						SetConsoleTextAttribute(stdOut, oldColor);
					}
				}
				cout << endl;
				continue;
			}
		}

		//Display an information message to the user
		cout << "[*] A trocar a peca 0 " << "com a peca " << user_choice << endl;

		//Swap the chose piece with the 0 piece
		b1 -> swapPieces(b1 -> getPiece(0, b1 -> board), b1 -> getPiece(user_choice, b1 -> board));

		//Display the board
		cout << *b1 << endl;
		//cout.clear();
	}

	//At this point the board is solved, so we stop the timer
	timer.stop_timer();

	//Display the tmie a user took to solve the puzzle
	show_formatted_time(timer, used_hints);


	//If user was playing in competition mode the program
	//should ask his/her data...
	if(game_mode == "C")
	{
		//Player object that contains the current player data
		Player player = ask_user_data(timer.elapsed_time());

		//If the user chose a old puzzle then we just add the current player object to the file
		if(type == "L")
		{
			vector<Player> players = files.get_all_players_file(file_to_load.c_str());
			if(players.size() > 0)
			{
				//New record for that puzzle?
				//If true display the new best time to the user!
				if(player.getTime() < players[0].getTime())
				{
					cout << "[INFO] Novo recorde: ";
					//Record in written in green color
					SetConsoleTextAttribute(stdOut, GREEN_TEXT);
					cout << player.getTime() << "s" << endl;
					//Back to the original color
					SetConsoleTextAttribute(stdOut, oldColor);
				}
			}
			//Add the player to the file
			files.add_player_to_file(file_to_load.c_str(), player);

			//Display an information message to the user
			cout << "[*] Os ficheiros foram actualizados com sucesso!" << endl;
		} else {
			//User chose "new puzzle"
			//We create a new file with the puzzle user just played and his/her information
			files.create_new_puzzle_file(original_board, files.get_number_of_puzzles(b1 -> getBoardDimension(), PUZZLE_NUMBERS) + 1, player);

			//Update the number of puzzles in PUZZLE_NUMBERS file
			files.update_puzzle(b1 -> getBoardDimension(), PUZZLE_NUMBERS);

			//Display an information message to the user
			cout << "[*] Os ficheiros foram actualizados com sucesso!" << endl;
		}
	} else if(game_mode == "T") {
		//Training mode
		//Just "reset" the game
		getchar();
		clear_screen();
		askUserStart();
	}
}


//Destructor...
Game::~Game()
{
	//Free the allocated memory
	delete b1;
}