#ifndef PIECE_H
#define PIECE_H
#include <iostream>
using std::ostream;


/**
 * Class Piece
 *
 *
 * This function represents each Piece in a Board
 * We have only implemented basic functions on this one
 *
 * Each Piece must have a line, a column and a number
 *
 *
 * NOTE: ALl the public functions are described in piece.cpp
*/


class Piece
{
private:

  //This variable holds the line of a Piece
  int line;

  //This variable holds the column of a Piece
  int column;

  //This variable holds the numbre of a Piece
  int number;

public:
  Piece(int l, int c, int n);
  virtual ~Piece();
  
  int getLine() const;
  void setLine(const int new_line);
  int getColumn() const;
  void setColumn(const int new_column);
  int getNumber() const;
  void setNumber(const int new_number);
  friend ostream& operator<< (ostream& o, const Piece& piece);
};

#endif // PIECE_H