#ifndef TIMER_H
#define TIMER_H
#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
using std::cout;
using std::endl;
using std::string;
using std::chrono::time_point;
using std::chrono::system_clock;
using std::chrono::duration_cast;
using std::time_t;



/**
 * Class Timer
 *
 *
 * This function works like a stopwatch when a player is solving a puzzle
 * We have only implemented the basic functions, like start and stop
 *
 *
 *
 * NOTE: This class uses the chrono library, part of the C++11
*/

class Timer {

private:
	//Variable responsible to record the start time
	time_point<system_clock> start_time;

	//Variable responsible to record the end time
	time_point<system_clock> end_time;

	//Returns true if the stopwatch is running
	bool running;

	//Variable responsible to hold the total number of seconds between start_time and end_time
	int total_seconds;

public:
	explicit Timer(bool start_now = false);
	void start_timer();
	void stop_timer(string optional_message = "");
	int elapsed_time();
};

#endif //TIMER_H