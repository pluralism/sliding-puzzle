#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <Windows.h>
#include "piece.h"
#include "player.h"
#include "board.h"
#include "files.h"
#include "timer.h"
#include "string_utils.h"
using std::cout;
using std::cin;
using std::endl;
using std::bad_alloc;

#define BLUE_TEXT			1
#define GREEN_TEXT			2
#define PUZZLE_NUMBERS		"puzzle_numbers.txt"



/**
 * Class Game
 *
 *
 * This class is one of the most important in the project
 * It is responsible for every aspect related with the game
 * - It loads stats from text files
 * - Ask users to choose a puzzle size
 * - Show all available puzzles
 * - Record user information in a text file
 * - Prompt user to choose an already solved puzzle and try to beat the top time
 * - Display the time a player took to solve a puzzle
 * - Much more...

 * NOTE: All functions declared as public are explained in game.cpp
*/


class Game {

private:
	//Variable that holds the number that the user chooses to swap with the 0
	string user_choice;

	//Variable that hold the game mode(T or C)
	string game_mode;

	//String_utils object(mainly used to deal with string formatting)
	string_utils str_utils;

	//vector of Piece that each time a Player plays it display the valid moves
	vector<Piece> valid_moves;

	//Variable that hold the dimensions of the board to be played
	vector<string> dimensions;

	//Board(vector of vectors of Piece) to be played
	vector<vector<Piece> > board;

	//This holds a copy of the original board to be written to a file in Competition mode
	vector<vector<Piece> > original_board;

	//Board object pointer
	Board *b1;

	//Files object to deal with file I/O
	Files files;

public:
	Game();
	void load_stats();
	void askUserStart();
	void prepareGame();
	void play_game(string mode, string type = "N", string file = "");
	Player ask_user_data(unsigned int time);
	void show_formatted_time(Timer t, const int used_hints);
	void show_all_puzzles(const int size, const int limit);
	void show_game_logo();
	string select_puzzle_from_files(const int size, const int limit);
	string ask_user_number();
	bool contains_invalid_char(string &s);
	void clear_screen();
	virtual ~Game();
};

#endif //GAME_H