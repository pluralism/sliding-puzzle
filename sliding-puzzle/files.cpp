#include "files.h"


/**
 * Default constructor
 * It does nothing
*/
Files::Files()
{

}


/**
 * This is a Compare used to sort the file numbers in PUZZLE_NUMBERS
 * The puzzles are sorted in descending order
*/
bool Files::sort_file_numbers(const Puzzle& first, const Puzzle& second)
{
	return atoi(first.total_played.c_str()) > atoi(second.total_played.c_str());
}


/**
 * This is a Compare used to the players by the time it took to solve a puzzle
 * The players are sorted in ascending order
*/
bool Files::sort_player_rank(const Player& first, const Player& second)
{
	return first.getTime() < second.getTime();
}


/**
 * This function check if a file exists
 *
 * filename - file to check
 *
 * Return true if exists
*/
bool Files::file_exists(const char *filename)
{
	if((_access(filename, 0)) != -1)
		return true;
	
	return false;
}


/**
 * Function used to create a file with name "filename"
 * fopen_s if the version of fopen with better security
 *
 * filename - name of the file to be created
 *
 * Returns true if the program created the filename, false otherwise
*/
bool Files::create_file(const char *filename)
{
	FILE *file;
	errno_t status;

	status = fopen_s(&file, filename, "w");

	if(status == 0)
	{
		fclose(file);
		return true;
	}

	return false;
}


/**
 * Write the standard sizes with total played 0 to the file "filename"
 * These stats are written to the standard sizes only!
 * Standard sizes are the following:
 * 3x3
 * 4x4
 * 5x5
 *
 *
 * filename - file where we'll be writing data
*/
void Files::create_zero_stats(const char *filename)
{
	ofstream file;

	try{
		file.open(filename);

		if(file.is_open())
		{
			for(int i = 3; i <= 5; i++)
			{
				Puzzle puzzle;
				ostringstream ss;
				ss << i;
				ss << "x";
				ss << i;
				puzzle.type = ss.str();
				puzzle.total_played = "0";

				//The data written is as the following:
				//"3x3	15"
				file << puzzle.type << "\t" << puzzle.total_played;

				if(i < 5)
					file << endl;
			}
		}
	}catch(ofstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}
}


/**
 * Function that check if a file contains the standard sizes
 * Standard sizes are the following:
 * 3x3
 * 4x4
 * 5x5
 *
 *
 * filename - file where we'll be reading data
*/
bool Files::file_contains_standard_sizes(const char *filename)
{
	ifstream fin;

	//Vector that holds each puzzle in the puzzle
	vector<Puzzle> puzzles;

	try {
		//Open the file
		fin.open(filename);

		if(fin.is_open())
		{
			//Read till the end of the file
			while(!fin.eof())
			{
				//Create a new puzzle object with type and total_played extracted from the file
				//"3x3	15" 
				//type -> 3x3
				//total_played -> 15
				Puzzle puzzle;
				fin >> puzzle.type;
				fin >> puzzle.total_played;

				//Add the Puzzle object to the vector
				puzzles.push_back(puzzle);
			}
		}

		//It can't have less than 3 puzzles sizes
		if(puzzles.size() < 3)
			return false;

		for(size_t i = 0; i < puzzles.size(); i++)
		{
			//Analyze the standard sizes
			if(puzzles[i].type[0] == '3' || puzzles[i].type[0] == '4' || puzzles[i].type[0] == '5')
			{
				//If there is no puzzles played for that puzzle something is wrong
				if(puzzles[i].total_played == "")
					return false;

				//Analyze each total_played
				//If it contains non digit chars return false(invalid data)
				for(size_t j = 0; j < puzzles[i].total_played.size(); j++)
				{
					if(!isdigit(puzzles[i].total_played[j]))
						return false;
				}
			}
		}

		//Close the file
		fin.close();

		//Everything was right in the file
		return true;
	} catch(ifstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		return false;
	}

	//We've got invalid data. Return false
	return false;
}


/**
 * Function that returns a vector of struct Puzzle defined in files.h
 * It reads all the puzzles from a file
 * Each puzzle has a size and a total_played associated with it
 *
 * filename - file to examine
*/
vector<Files::Puzzle> Files::get_all_puzzles(const char *filename)
{
	vector<Files::Puzzle> puzzles;
	ifstream fin;

	if(file_exists(filename))
	{
		try {
			//Open the file
			fin.open(filename);

			if(fin.is_open())
			{
				//Read till the end of the file
				while(!fin.eof())
				{
					//Create a new puzzle object with type and total_played extracted from the file
					Puzzle puzzle;
					fin >> puzzle.type;
					fin >> puzzle.total_played;

					//Add the Puzzle object to the vector
					puzzles.push_back(puzzle);
				}
			}

			//Sort the puzzles vector according to the function defined above(sort_file_numbers)
			sort(puzzles.begin(), puzzles.end(), sort_file_numbers);

			//Close the file
			fin.close();
		}catch(ifstream::failure ex) {
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}

	//Return the vector of puzzles
	return puzzles;
}


/**
 * Function that updates the file that contains the stats of all the puzzles
 * It updates the puzzle of size "size" by incrementing the total_played by 1
 *
 * size - size of the puzzle
 * filename - file to analyze
*/
void Files::update_puzzle(const int size, const char *filename)
{
	//Vector that contains all the puzzle from the file "filename"
	vector<Files::Puzzle> puzzles = get_all_puzzles(filename);

	//Variable that checks if the puzzle_size was found int filename
	bool found = false;
	ofstream file;
	int puzzle_total;

	try{
		file.open(filename, ios::trunc);

		if(file.is_open())
		{
			if(puzzles.size() > 0)
			{
				for(size_t i = 0; i < puzzles.size(); i++)
				{
					file << puzzles[i].type << "\t";

					//Great! We've found the size that we're looking for 
					if(str_utils.extract_size(puzzles[i].type) == size)
					{
						//Increment the total_played by 1
						puzzle_total = atoi(puzzles[i].total_played.c_str()) + 1;

						//we found the puzzle
						found = true;
					}
					else
						puzzle_total = atoi(puzzles[i].total_played.c_str()); //Just keep reading...

					string size = to_string(puzzle_total);
					
					//If we're in the last puzzle we should not add a new line
					if(i == puzzles.size() - 1)
						file << size;
					else
						file << size << endl;
				}

				//Puzzle not found, means it is a new size
				//Add it to the file
				if(!found)
				{
					Puzzle new_size;
					new_size.type = to_string(size) + "x" + to_string(size);
					new_size.total_played = "0";
					file << endl << new_size.type << "\t" << new_size.total_played;
				}
			}
		}

		//Close the file
		file.close();
	}catch(ofstream::failure ex) {
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}
}


/**
 * Function that returns the total number of puzzles of size "size"
 * in filename
 * 
 * size - size we're looking for
 * filename - file where we're analyzing the file
*/
int Files::get_number_of_puzzles(const int size, const char *filename)
{
	ifstream fin;
	unsigned int total_puzzles = 0;

	if(file_exists(filename))
	{
		try {
			fin.open(filename);

			if(fin.is_open())
			{
				//Read till the end of the file
				while(!fin.eof())
				{
					//Create a new puzzle object with type and total_played extracted from the file
					Puzzle puzzle;
					fin >> puzzle.type;
					fin >> puzzle.total_played;

					//We've found the size we want
					if(str_utils.extract_size(puzzle.type) == size)
					{
						total_puzzles = atoi(puzzle.total_played.c_str());

						//Return the number of puzzles
						return total_puzzles;
					}
				}
			}

			//Close the file
			fin.close();
		}catch(ifstream::failure ex) {
			//Should not happen
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}

	//Not found!
	return -1;
}



/**
 * This function writes a puzzle to the file "filename"
 *
 * filename - file we're writting for
 * board - the board we want to write
*/
void Files::write_puzzle_data(const char *filename, vector<vector<Piece> > &board)
{
	vector<vector<Piece> > b1 = board;
	Board board_b1;

	if(file_exists(filename))
	{
		ofstream fout;
		try {
			//Open the file
			fout.open(filename);

			if(fout.is_open())
			{
				for(size_t i = 0; i < b1.size(); i++)
				{
					vector<Piece> row;

					for(size_t j = 0; j < b1[i].size(); j++)
					{
						//Make sure we're reading with the right width
						//If we didn't do that the program could easily crash
						//If the number of digits of the current number(2 for the number 16 for example)
						//is less than the maximum number of digits in that board
						//then we have to adjust the width with the setw(int n) function by adding till 
						//we get a width of the maximum number of digits
						if(str_utils.getNumberOfDigits(b1[i][j].getNumber()) < str_utils.getNumberOfDigits((int)pow(b1.size(), 2)))
						{
							int width = str_utils.getNumberOfDigits((int)pow(b1.size(), 2)) - str_utils.getNumberOfDigits(b1[i][j].getNumber()) + 1;
							fout << setw(width);

							//Instead of writing 0 we write a space for it
							if(b1[i][j].getNumber() == 0)
								fout << " ";
							else
								fout << b1[i][j]; //Not zero? Write the number
						} else {
							//Normal formatting. No need to adjust the width
							fout << setw(str_utils.getNumberOfDigits((int)pow(b1.size(), 2)));

							if(b1[i][j].getNumber() == 0)
								fout << " ";
							else
								fout << b1[i][j];
						}

						//We shouldn't write a space in the last number of each line
						if(j != b1[i].size() - 1)
							fout << " ";
					}
					//Go to the next line
					fout << endl;
				}
			}

			//Close the file
			fout.close();
		}catch(ofstream::failure ex) {
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}
}


/**
 * Returns the size of a puzzle that is written is file "filename"
 *
 * filename - the file we'll be searching
*/
int Files::get_puzzle_file_size(const char *filename)
{
	ifstream fin;
	string size = "";
	string file(filename);

	//We only to read one line(puzzle is supposed to be NxN)
	for(size_t i = 0; i < file.size(); i++)
	{
		//Found a digit
		if(isdigit(file[i]))
		{
			//While is a digit keep adding chars to the string
			while(isdigit(file[i]))
			{
				//Add to string and increment i
				size += file[i++];
			}
			//Found it, no need to keep examining
			break;
		}
	}

	//Return the int version of the string "file"
	return atoi(size.c_str());
}


/**
 * Function that displays an error message to the username
 * 
 * filename - file used to show the error message
*/
void Files::show_file_error(const char *filename)
{
	//File "filename" not found
	cerr << "[!!] Erro! Nao existe um puzzle no ficheiro " << filename << endl;
	cerr << "[!!] Por favor verifique o ficheiro(o puzzle pode nao estar escrito na primeira linha). A fechar o programa..." << endl;
	getchar();

	//Close the program...
	exit(EXIT_FAILURE);
}


/**
 * Function that returns the number of players in a file "filename"
 * The players are return into a vector of Player object
 *
 * filename - the file we'll be searching for players objects
*/
vector<Player> Files::get_all_players_file(const char *filename)
{
	ifstream fin;
	string line;
	vector<Player> players;

	//Variable that holds the size of a puzzle
	int puzzle_size = get_puzzle_file_size(filename);

	//Puzzle not found
	if(puzzle_size == 0)
	{
		//Display an error message
		show_file_error(filename);
	}

	if(file_exists(filename))
	{
		try{
			//Open the file
			fin.open(filename);

			if(fin.is_open())
			{
				//We can ignore the puzzle_size + 1 lines
				//Example:
				//1 2 3 
				//4 5 6
				//7 8 0
				//
				//Andre .......
				//Here we ignore 3 + 1 lines, so the pointer of the file is now in the players
				int ignore_lines = puzzle_size + 1;

				for(int i = 0; i < ignore_lines; i++)
					fin.ignore(INT_MAX, '\n');

				//Read till the end of the file
				while(!fin.eof())
				{
					getline(fin, line);
					
					//If the line is not empty, add the player to the vector
					//Else ignore that line
					if(!line.empty())
						players.push_back(str_utils.extract_player_data(line));
				}
			}

			//Close the file
			fin.close();
		}catch(ifstream::failure ex) {
			//Should not happen
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}

	return players;
}


/**
 * Function that returns all the numbers in one line of the file "filename"
 * It is able to guess the 0 position with the max_diff parameter
 *
 * filename - file where we'll be searching
 * puzzle_size - the size of the puzzle
 * max_diff - used to guess the 0 pos
 * pos - line to search
 *
 * Returns a vector of int with the numbers in that line
*/
vector<int> Files::get_number_line(const char *filename, const int puzzle_size, const int max_diff, const int pos)
{
	vector<int> numbers;
	string line;
	ifstream fin;

	try{
		fin.open(filename);

		if(fin.is_open())
		{
			for(int i = 0; i < puzzle_size; i++)
			{
				getline(fin, line);

				//We're in the right line
				if(i == pos)
				{
					string cur_number;
					int spaces = 0;

					//Analyze each char of the line
					for(size_t j = 0; j < line.size(); j++)
					{
						//Good. We've found a digit
						if(isdigit(line[j]))
						{
							//Read till we find a non digit char and add that char to cur_number
							while(isdigit(line[j]) && j < line.size())
								cur_number += line[j++];


							//Add the current number to the vector
							numbers.push_back(atoi(cur_number.c_str()));

							//Clear the current_number, so we can analyze another
							cur_number = "";
						}

						//Found a space, maybe we find the 0
						if(isspace(line[j]))
						{
							while(isspace(line[j]) && j < line.size())
							{
								j++;
								spaces++;
							}

							//This is the 0! Add the 0 to the vector
							if(spaces == max_diff || spaces == max_diff - 1)
								numbers.push_back(0);

							//Reset the number of spaces
							spaces = 0;
							//Go to the previous char
							j--;
						}
					}
				}
			}
		}

	}catch(ifstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}

	return numbers;
}


/**
 * Function that return the number of the line where the 0 is in file "filename"
 *
 * filename - file that we'll be analyzing
 * file_size - the size we're looking for
*/
int Files::get_zeros_pos(const char *filename, const int puzzle_size)
{
	ifstream fin;
	string line;
	vector<int> line_numbers;
	int l = 0;

	//Used to locate the 0
	int max_diff = str_utils.getNumberOfDigits((int)pow(puzzle_size, 2)) + 2;

	try{
		fin.open(filename);

		if(fin.is_open())
		{
			for(int i = 0; i < puzzle_size; i++)
			{
				//Analyze every line of the file
				getline(fin, line);

				for(size_t j = 0; j < line.size(); j++)
				{
					//Found a space, maybe 0 is here!
					if(isspace(line[j]))
					{
						int k = j;
						//Keep counting the number of spaces till we find a non space char
						while(isspace(line[k]))
							k++;

						//Zero is here!
						if(k - j == max_diff || k - j == max_diff - 1)
							l = i;
					}
				}
			}
		}

		fin.close();
	}catch(ifstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}

	//Return the line where the 0 is
	return l;
}


/**
 * Function used to extract a puzzle from a file "filename"
 * 
 * filename - file where we'll be extracting the puzzle
 *
 * It returns a vector of vectors of Piece(same as Board) contaning all the Pieces correctly numbered
*/
vector<vector<Piece> > Files::extract_puzzle_file(const char *filename)
{
	ifstream fin;

	//Variable that holds the size of the puzzle in "filename"
	int puzzle_size = get_puzzle_file_size(filename);
	vector<vector<Piece> > board;

	//Variable that holds the position of the 0 in the puzzle
	int zero = get_zeros_pos(filename, puzzle_size);

	//Used to find the 0
	int max_diff = str_utils.getNumberOfDigits((int)pow(puzzle_size, 2)) + 2;
	vector<int> numbers;
	
	try{
		//Open the file
		fin.open(filename);

		if(fin.is_open())
		{
			for(int i = 0; i < puzzle_size; i++)
			{
				vector<Piece> row;

				//Vector of Piece with the numbers in the line i
				numbers = get_number_line(filename, puzzle_size, max_diff, i);

				for(int j = 0; j < puzzle_size; j++)
				{
					row.push_back(Piece(i, j, numbers[j]));
				}

				//Push a new row to the Board(vector of vectors of Piece) board
				board.push_back(row);
			}
		}

	}catch(ifstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}

	//Returns a new board
	return board;
}


/**
 * This function is used to add a new Player object to the file "filename"
 * The player is first added to a vector and the vector is sorted according to the Compare
 * sort_player_rank
 * After that each player is written to the file "filename"
 *
 * filename - the file we'll be writting the Player object
 * player   - the player we want to write to the file
*/
void Files::add_player_to_file(const char *filename, Player &player)
{
	ofstream fout;

	//Vector that holds all the players in the file "filename"
	vector<Player> players = get_all_players_file(filename);

	//Holds the puzzle that is in the "filename" file
	vector<vector<Piece> > board = extract_puzzle_file(filename);

	//Add the new player
	players.push_back(player);

	//And sort the vector
	sort(players.begin(), players.end(), sort_player_rank);

	//We've got some data.. We're ready to go!
	if(players.size() > 0)
	{
		//This variable holds the size of the puzzle
		int puzzle_size = get_puzzle_file_size(filename);

		try{
			//Open the file
			fout.open(filename);

			if(fout.is_open())
			{
				for(size_t i = 0; i < board.size(); i++)
				{
					vector<Piece> row;

					for(size_t j = 0; j < board[i].size(); j++)
					{
						//[NOTE]This is already explained in a function above!!!
						//Make sure we're reading with the right width
						//If we didn't do that the program could easily crash
						//If the number of digits of the current number(2 for the number 16 for example)
						//is less than the maximum number of digits in that board
						//then we have to adjust the width with the setw(int n) function by adding till 
						//we get a width of the maximum number of digits
						if(str_utils.getNumberOfDigits(board[i][j].getNumber()) < str_utils.getNumberOfDigits((int)pow(board.size(), 2)))
						{
							int width = str_utils.getNumberOfDigits((int)pow(board.size(), 2)) - str_utils.getNumberOfDigits(board[i][j].getNumber()) + 1;
							fout << setw(width);

							//We should not write the 0, instead we write a space
							if(board[i][j].getNumber() == 0)
								fout << " ";
							else
								fout << board[i][j]; //Else we write the original number
						} else {
							//There's no need to format the width
							fout << setw(str_utils.getNumberOfDigits((int)pow(board.size(), 2)));

							//Found a space? 
							//If true write a space
							if(board[i][j].getNumber() == 0)
								fout << " ";
							else
								fout << board[i][j]; //Else write the original number
						}

						//Are we in the last column?
						//If no write a space, else do nothing
						if(j != board[i].size() - 1)
							fout << " ";
					}
					//Are we in the last line?
					//If no change line, else just keep going
					if(i != board.size() - 1)
						fout << endl;
				}
				fout << endl;
				
				//In this loop we write the top 10 players for that puzzle
				for(size_t i = 0; i < (players.size() > 10 ? 10 : players.size()); i++)
					fout << endl << players[i];
			}

			//Close the file
			fout.close();
		}catch(ofstream::failure ex) {
			//Should not happen
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}
}


/**
 * Function used to write a new player object to the file "filename"
 *
 * filename - file we want to write to 
 * player   - the object we want to write
*/
void Files::write_player_data(const char *filename, Player &player)
{
	ofstream fout;

	//Vector that holds all the player objects in the file "filename"
	vector<Player> players = get_all_players_file(filename);

	if(file_exists(filename))
	{
		try {
			fout.open(filename, ios::app);

			//Write the player object 
			fout << endl << player;


			fout.close();
		}catch(ofstream::failure ex) {
			//Should not happen
			cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
		}
	}
}


/**
 * This function is used when we want to create a new file
 * with a board and a player object
 *
 * b1 - the board we want to write to the newly created file
 * puzzle_number - used to generate a valid file name(holds the number of puzzle)
 * player - The player object we want to write to that file
*/
void Files::create_new_puzzle_file(vector<vector<Piece> > &b1, int puzzle_number, Player &player)
{
	int board_width = b1.size();
	string size;

	//It holds the size of the puzzle
	size = str_utils.format_puzzle_number(puzzle_number);

	//This variable holds the correct file name we will create
	string file_name = FILE_BASE + to_string(board_width) + "x" + to_string(board_width) + "_" + size + ".txt";

	//File alredy exists
	if(file_exists(file_name.c_str()))
	{
		//Display an error message to the user
		cout << "[!!] O ficheiro " << "\"" << file_name << "\" ja existe!" << endl << endl;
	} else {
		//If the file doesn't exist, then we create a new file
		//and the vector of vectors of Piece b1 and the player object are written to that file
		create_file(file_name.c_str());

		//Write the puzzle
		write_puzzle_data(file_name.c_str(), b1);
		//Write the player object
		write_player_data(file_name.c_str(), player);
	}
}


/**
 * Function that is used to display all the content of a file
 *
 * filename - file we want to display to the user
*/
void Files::show_file(const char *filename)
{
	ifstream fin;
	string line;

	try {
		fin.open(filename);	

		if(fin.is_open())
		{
			//Read till the end of the file
			while(!fin.eof())
			{
				getline(fin, line);

				//Display each line to the user
				cout << line << endl;
			}
		}

		//Close the file
		fin.close();
	}catch(ifstream::failure ex) {
		//Should not happen
		cerr << "[!] Ocorreu um erro. Ficheiro " << filename << endl;
	}
}