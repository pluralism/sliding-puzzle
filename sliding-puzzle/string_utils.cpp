#include "string_utils.h"


//Default constructor of the class, it does nothing
string_utils::string_utils()
{

}


/**
 * Extract the digits from a given string
 * This function is very important
 * It is the responsible to formatting a string s in the following form:
 *
 * "asdasd2sadxoiaaood2" returns "2x2"
 * 
 * s - string to search
 * pos - searching till this position

 * It returns a vector of string containing the digits in the string s
*/
vector<string> string_utils::extract_digits(const string &s, int pos) const
{
  vector<string> t;
  string l;
  
  for(int i = 0; i < pos; i++)
  {
	//If the current char is a digit then we add it to the temporary string l
    if(isdigit(s[i]))
      l += s[i];
  }

  //Should not happen.
  //No digits were found.
  //Assuming the the default size(3)
  if(l == "")
	  l.push_back('3');
 
  t.push_back(l);

  //Clear the string
  l = "";

  //Here we'll search for the second part of the size
  //In a string "33x123" we would be searching for the 123 part
  for(unsigned int i = pos + 1; i < s.size(); i++)
  {
	//If the current char is a digit then we add it to the temporary string l
    if(isdigit(s[i]))
      l += s[i];
  }

  //Should not happen.
  //No digits were found.
  //Assuming the the default size(3)
  if(l == "")
	  l.push_back('3');

  t.push_back(l);
    
  //Return the vector that contains the size of vector
  return t;
}



/**
 * After analyzing the string s with the function above
 * it returns the formatted string(digits only)
 *
 * The numbers are returned in a vector
*/
vector<string> string_utils::format_string(const string &s) const
{
  //The delimitor is 'x' or 'X'
  //In the start we still haven't found it
  bool found = false;

  //Try to locate the delimitor in the string
  int delimiter_index = s.find('x');
  vector<string> digits;
  
  //If the delimiter was found then found is now true and we extract the digits of the string
  //If not we will now try with 'X'
  if(delimiter_index != string::npos)
  {
    found = true;
    digits = extract_digits(s, delimiter_index);
  } else {
    found = true;
    delimiter_index = s.find('X');
    digits = extract_digits(s, delimiter_index);
  }
  
  //'x' and 'X' were not found, so there's no need to keep the application opened
  //Print an error message to the user and exit
  if(!found)
  {
    cerr << "[!] Nao foi possivel encontrar o delimitador!" << endl;
	getchar();
    exit(EXIT_FAILURE);
  }
  
  //If we have found the delimitor return the vector of string containing the digits
  return digits;
}



/**
 * Function that converts a string into an int
 *
 * s - string to convert
 * 
 * We used c_str() because atoi takes a const char* argument
*/
int string_utils::convertString(const string& s) const
{
  return atoi(s.c_str());
}


/**
 * Function that converts a char into an int
 *
 * c - char to convert
*/
int string_utils::char_to_int(char c)
{
	//Conversion is based in the ASCII codes
	return (c - '0');
}



/**
 * Converts a string into an int
 * Although this only accepts a formatted string
 * while the one defined above accepts unformatted strings
 *
 * s - string to convert
*/
int string_utils::extract_size(const string& s)
{
	//f is a substring of s 
	//starts at position 0 and the copy the chars till it finds an x
	string f = s.substr(0, s.find("x"));

	//Convert the string to an int
	return atoi(f.c_str());
}


/**
 * This function takes a string as an argument
 * and returns a Player object
 *
 * line - string to be analyzed
 *
 * A line is the following format
 * "         Andre 18 M   34"
*/
Player string_utils::extract_player_data(string &line)
{
	unsigned int i = 0;
	string name;
	string age;
	char sex;
	string time;
	bool found_letter = false;

	for(i; i < line.size(); i++)
	{
		//We know that the first thing we'll find is the player name
		if(!isspace(line[i]))
			found_letter = true; //Found the first letter of the player name

		if(found_letter)
		{
			//If is a digit it means we have now found the player age
			if(isdigit(line[i]))
				break;

			//Keep adding chars to the string name till we find a digit
			name += line[i];
		}
	}


	for(unsigned int i = name.size() - 1; i >= 0; i--)
	{
		if(!isspace(name[i]))
			break;

		//Erase all the spaces in the string
		name.erase(i, 1);
	}


	for(i; i < line.size(); i++)
	{
		//We have found a space, stop the search
		if(isspace(line[i]))
			break;

		//Else add the current char to the string age
		age += line[i];
	}

	//Skip the space
	i++;

	//sex is represented by a single char
	//copy it to the sex char and increment i
	sex = line[i++];


	for(i; i < line.size(); i++)
	{
		//The last thing we want is the player time
		//Extract it and place in the time variable
		if(isdigit(line[i]))
			time += line[i];
	}


	//Returns a new Player object with the extracted data by the function
	return Player(name, atoi(age.c_str()), sex, atoi(time.c_str()));
}


/**
 * Function that returns a string in a special format
 *
 * number - number that we want to convert to string
 * 
 * We know that the txt files are in the following format:
 * "puzzle_3x3_001.txt"
 *
 * This is responsible for returning the correct puzzle number in a string
 *
 *
 * NOTE: to_string(int n) is a C++11 function, so it should be compiled with VS2012
*/
string string_utils::format_puzzle_number(const int number)
{
	string size;

	//The final number must have 3 chars
	//So, if the number is less than 10(1 char) it adds two 0
	if(number < 10)
		size = "00" + to_string(number);
	else if(number < 100) //If is less than 100 is add one 0
		size = "0" + to_string(number);
	else
		size = to_string(number); //Original number, it has 3 digits

	return size;
}


/**
 * Converts a string into the corresponding uppercase string.
 *
 * s - string to convert
*/
void string_utils::convert_upper(string &s)
{
	for(size_t i = 0; i < s.size(); i++)
		s[i] = toupper(s[i]);
}


/**
 * Function that returns the number of digits in an int number
 * using the log10(int n) function.
 *
 * number - number we are analyzing
*/
int string_utils::getNumberOfDigits(int number) const
{
  if(number == 0)
    return 1;
  
  return (int)log10(number) + 1;
}


string_utils::~string_utils()
{

}