#ifndef STRING_UTILS_H
#define STRING_UTILS_H
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include "player.h"
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::stringstream;
using std::to_string;
using std::cerr;



/**
 * Class string_utils
 * 
 * This class contains several functions to work with strings
 * and vector of strings
 *
 * It is able to format strings, convert between different types
 * extract data from a file with Players
 * extract the size of a puzzle
 *
 *
 *
 * NOTE: All the public functions are explained in string_utils.cpp
*/


class string_utils
{
public:
  string_utils();
  int char_to_int(char c);
  vector<string> format_string(const string &s) const;
  vector<string> extract_digits(const string &s, int pos) const;
  Player extract_player_data(string &line);
  int convertString(const string& s) const;
  string format_puzzle_number(const int number);
  void convert_upper(string &s);
  int getNumberOfDigits(int number) const;
  int extract_size(const string& s);
  virtual ~string_utils();
};

#endif // STRING_UTILS_H