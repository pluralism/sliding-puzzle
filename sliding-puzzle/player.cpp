#include "player.h"
#include <vector>


/**
 * Default constructor of the class
 * It takes 4 arguments:
 *
 * name
 * age
 * sex
 * time
 *
 * The first thing it does is to format the name of the player
*/
Player::Player(string name, unsigned int age, char sex, unsigned int time):
  name(name),
  age(age),
  sex(sex),
  time(time)
{
	format_name();
}


/**
 * Function that checks if the age is valid
 * 0 < age < 100 -> valid
*/
bool Player::validAge()
{
  return (age > 0 && age < 100);
}



//Returns the time a player took to solve a puzzle
const unsigned int Player::getTime() const
{
	return time;
}


/**
 * If the age is valid it returns the age of the current player
 *
 * If not it returns 0(error)
*/
unsigned int Player::getAge()
{
  if(validAge())
    return age;

  return 0;
}


//Returns the name of the current player
string Player::getName() const
{
  return name;
}


//Return the sex of the current player
char Player::getSex() const
{
  return sex;
}


/**
 * This function is responsible to format a player name.
 * Example:
 *
 * andre pinheiro would be Andre Pinheiro
 * It doesn't return anything, instead it modifies the member of the class
*/
void Player::format_name()
{
	int name_size = name.size();

	if(name_size > 0)
	{
		if(islower(name[0]))
			name[0] = toupper(name[0]);

		for(int i = 0; i < name_size; i++)
		{
			//0x20 -> hex code for space char
			if(name[i] == 0x20 && i + 1 < name_size)
			{
				if(islower(name[i + 1]))
					name[i + 1] = toupper(name[i + 1]);
			}
		}
	}
}


/**
 * Returns an ostream object and it prints the player data
 * It's also used when we want to write the
 * information from the player to a text file
*/
ostream& operator<< (ostream& o, Player& player)
{
  if(player.validAge())
  {
	  o << setw(20) << player.getName() << setw(3) << player.getAge() << setw(2) << player.getSex()
		<< setw(5) << player.getTime();
  } else {
    o << "[!] Invalid data!";
    exit(EXIT_FAILURE);
  }

  return o;
}


//Destructor...
Player::~Player()
{

}