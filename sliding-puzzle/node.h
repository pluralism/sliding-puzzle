#ifndef NODE_H
#define NODE_H
#include <string>
#include <vector>
#include "piece.h"
using std::string;
using std::vector;


/**
 * Class Node
 *
 *
 *
 * This class is responsible to manage each node in a Tree
 * Each node contains a state and a solution
 * The class also includes a swap function to swap two letters in a solution
 *
 *
 * NOTE: All the functions declared as public are described in node.cpp
*/

class Node
{
private:
   
  //This variable holds the current solution of the board
  string solution;

  //This variable holds the current state of the board
  vector<int> state;
  
public:
  Node(vector<int> state, string solution);
  string getSolution();
  vector<int> getState();
  void setSolution(string solution);
  void swapLetters(const int l1, const int l2);
  virtual ~Node();
};

#endif // NODE_H