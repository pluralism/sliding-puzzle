## Sliding puzzle ##

---------------------------------------

This is a command line version of the popular 8/15 game. The program is currently under development and it has been suffering constant updates. Once we have a stable version we'll add it to the download section.


__Main features__

* Play a puzzle of any size
* Save the stats to a text file(name, age, sex, time it took to solve the puzzle)
* Hints system



__To do list:__

* Create the competition/training mode
* Load a paused puzzle
* Sort the times in the text file so we can create a rank